/**
 * 5L>2M>2H xx SD, j.5L>j.5M>JC>j.5L>j.5M>j.2H xx 214H xx 236L+M
 *
 * > chain
 * xx cancel (i.e. cancel into super)
 *
 * L+M - dragon rush button
 * M+H - super dash button
 * (j.)([0-9])(L|M|H|S|L+M) - move
 * move | JC            - block
 * block (xx (SD|block))
 */

const Language = require("./parser");
const PLATFORMS = require("./outputs");

const NUMPAD = {
  1: "↙",
  2: "↓",
  3: "↘",
  4: "←",
  5: "",
  6: "→",
  7: "↖",
  8: "↑",
  9: "↗"
};

const compose = (f, g) => x => f(g(x));
const map = f => l => l.map(f);
const flatMap = f => l => l.reduce((acc, i) => [...acc, ...f(i)], []);
const join = s => i => i.join(s);
const trim = i => i.replace(/\s/g, "");

const parse = Language.Combo.parse.bind(Language.Combo);
// strip any whitespace so that we don't have to care about it in the parser
const Parser = compose(parse, trim);

const makeString = compose(
  join("\n"),
  map(({ name, combo }) =>
    compose(s => `${name}: ${join(", ")(s)}`, map(join(" > ")))(combo)
  )
);

const translate = input => {
  const { status, value: combo } = Parser(input);

  if (!status) {
    // todo: better error-handling
    console.error("yo that didn't work");
    return;
  }

  const translated = PLATFORMS.map(({ name, mappings }) => ({
    name,
    combo: map(
      flatMap(
        map(({ Numbers, Button }) => {
          const numberstring = Numbers.map(n => NUMPAD[n]).join("");
          const button = mappings[Button] || "";

          return `${numberstring}${button}`;
        })
      )
    )(combo) // maybe we don't care about the cancel? (xx)
  }));

  return makeString(translated);
};

module.exports = translate;

const input =
  "IAD>L>5L>2M>2H xx SD, j.5L>j.5M>JC>j.5L>j.5M>j.2H xx 214H xx 236L+M xx 2H";

console.log(input);
console.log(translate(input));
