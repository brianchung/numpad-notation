const P = require("parsimmon");

const {
  LIGHT,
  MEDIUM,
  HEAVY,
  SPECIAL,
  DRAGON_RUSH,
  SUPER_DASH,
  ASSIST_ONE,
  ASSIST_TWO,
  JUMP_CANCEL,
  IAD
} = require("./symbols");

const passthrough = P.of("");
const Optional = p => p.or(passthrough);
const NumSequence = P.digits.map(d => d.split("").map(Number));

module.exports = P.createLanguage({
  Combo: ({ MoveString }) => MoveString.sepBy(P.string(",")),

  MoveString: ({ Cancel, Input, Chain }) => Input.sepBy(Chain).sepBy(Cancel),

  // a single input, i.e j.5L or
  Input: ({ Button, Jump }) =>
    P.seqObj(Optional(Jump), ["Numbers", NumSequence], ["Button", Button]),

  // a buttonm, i.e. L, S, JC
  // ordering here matters. more specific first. so that L+M doesn't match L rule first
  Button: ({
    DragonRush,
    SuperDash,
    JumpCancel,
    Light,
    Medium,
    Heavy,
    Special,
    IAD
  }) =>
    P.alt(
      DragonRush,
      SuperDash,
      JumpCancel,
      Light,
      Medium,
      Heavy,
      Special,
      IAD
    ),

  Light: () =>
    P.string("L")
      .trim(P.optWhitespace)
      .map(() => LIGHT)
      .desc("Light"),
  Medium: () =>
    P.string("M")
      .trim(P.optWhitespace)
      .map(() => MEDIUM)
      .desc("Medium"),
  Heavy: () =>
    P.string("H")
      .trim(P.optWhitespace)
      .map(() => HEAVY)
      .desc("Heavy"),
  Special: () =>
    P.string("S")
      .trim(P.optWhitespace)
      .map(() => SPECIAL)
      .desc("Special"),
  DragonRush: () =>
    P.alt(P.string("L+M"), P.string("DR"))
      .trim(P.optWhitespace)
      .map(() => DRAGON_RUSH)
      .desc("Dragon Rush"),
  SuperDash: () =>
    P.alt(P.string("M+H"), P.string("SD"))
      .trim(P.optWhitespace)
      .map(() => SUPER_DASH)
      .desc("Super Dash"),
  IAD: () =>
    P.string("IAD")
      .trim(P.optWhitespace)
      .map(() => IAD)
      .desc("Instant air dash"),
  // todo
  //   AssistOne: () => P.string('') ,
  //   AssistTWO: () => P.string('') ,

  Jump: () =>
    P.string("j.")
      .trim(P.optWhitespace)
      .map(() => 8)
      .desc("Jump"),
  JumpCancel: () =>
    P.string("JC")
      .trim(P.optWhitespace)
      .map(() => JUMP_CANCEL)
      .desc("Jump Cancel"),

  Chain: () => P.string(">").desc("Move Chain"),
  Cancel: () => P.string("xx").desc("Move Cancel")
});
