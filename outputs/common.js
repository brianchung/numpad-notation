const {
  DRAGON_RUSH,
  SUPER_DASH,
  ASSIST_ONE,
  ASSIST_TWO,
  JUMP_CANCEL,
  IAD
} = require("./../symbols");

module.exports = {
  [DRAGON_RUSH]: "R1",
  [SUPER_DASH]: "R2",
  [ASSIST_ONE]: "L1",
  [ASSIST_TWO]: "L2",
  [JUMP_CANCEL]: "↑",
  [IAD]: "↑→→"
};
