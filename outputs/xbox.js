const common = require("./common");

const { LIGHT, MEDIUM, HEAVY, SPECIAL } = require("./../symbols");

module.exports = {
  ...common,
  [LIGHT]: "X",
  [MEDIUM]: "Y",
  [HEAVY]: "B",
  [SPECIAL]: "A"
};
