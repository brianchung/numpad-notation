const PS4 = require("./ps4");
const XBOX = require("./xbox");

module.exports = [
  { name: "PS4", mappings: PS4 },
  { name: "Xbox One", mappings: XBOX }
];
