module.exports = {
  LIGHT: Symbol("light"),
  MEDIUM: Symbol("medium"),
  HEAVY: Symbol("heavy"),
  SPECIAL: Symbol("special"),
  DRAGON_RUSH: Symbol("dragon_rush"),
  SUPER_DASH: Symbol("super_dash"),
  ASSIST_ONE: Symbol("assist_one"),
  ASSIST_TWO: Symbol("assist_two"),
  JUMP_CANCEL: Symbol("jump_cancel"),
  IAD: Symbol("iad")
};
