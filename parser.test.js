import test from "ava";
import Parser from "./parser";
import {
  LIGHT,
  MEDIUM,
  HEAVY,
  SPECIAL,
  DRAGON_RUSH,
  SUPER_DASH,
  IAD,
  JUMP_CANCEL
} from "./symbols";

const macro = (t, parser, input, expected) =>
  t.deepEqual(Parser[parser].parse(input).value, expected);

macro.title = (providedTitle, parser, input, expected) =>
  `${providedTitle} ${input} = ${expected.toString()}`.trim();

test(macro, "Light", "L", LIGHT);
test(macro, "Medium", "M", MEDIUM);
test(macro, "Heavy", "H", HEAVY);
test(macro, "Special", "S", SPECIAL);

test(macro, "DragonRush", "L+M", DRAGON_RUSH);
test(macro, "DragonRush", "DR", DRAGON_RUSH);

test(macro, "SuperDash", "M+H", SUPER_DASH);
test(macro, "SuperDash", "SD", SUPER_DASH);

test(macro, "IAD", "IAD", IAD);

test(macro, "Jump", "j.", 8);
test(macro, "JumpCancel", "JC", JUMP_CANCEL);

test(macro, "Chain", ">", ">");
test(macro, "Cancel", "xx", "xx");

test(macro, "Input", "236L", {
  Numbers: [2, 3, 6],
  Button: LIGHT
});

test("With optional jump", macro, "Input", "j.236H", {
  Numbers: [2, 3, 6],
  Button: HEAVY
});

test(macro, "MoveString", "IAD>L>5M>236S xx SD", [
  [
    {
      Numbers: [],
      Button: IAD
    },
    {
      Numbers: [],
      Button: LIGHT
    },
    {
      Numbers: [5],
      Button: MEDIUM
    },
    {
      Numbers: [2, 3, 6],
      Button: SPECIAL
    }
  ],
  [
    {
      Numbers: [],
      Button: SUPER_DASH
    }
  ]
]);

test(macro, "Combo", "IAD>L>5L>2M>2H xx SD, j.5L>j.5M>j.236S", [
  [
    [
      {
        Numbers: [],
        Button: IAD
      },
      {
        Numbers: [],
        Button: LIGHT
      },
      {
        Numbers: [5],
        Button: LIGHT
      },
      {
        Numbers: [2],
        Button: MEDIUM
      },
      {
        Numbers: [2],
        Button: HEAVY
      }
    ],
    [
      {
        Numbers: [],
        Button: SUPER_DASH
      }
    ]
  ],
  [
    [
      {
        Numbers: [5],
        Button: LIGHT
      },
      {
        Numbers: [5],
        Button: MEDIUM
      },
      {
        Numbers: [2, 3, 6],
        Button: SPECIAL
      }
    ]
  ]
]);
